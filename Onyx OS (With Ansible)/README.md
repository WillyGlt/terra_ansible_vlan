# Deploy Vlans on Vcenter and Mellanox switch (Onyx OS) with Terraform and Ansible

### Replace the placeholders with the appropriate values on vcenter_vlan.tf :

`<vcenter_username>`

`<vcenter_password>`

`<vcenter_server>`

`<vcenter_dvswitch_uuid>`

`<vcenter_dvswitch_name>`

### Install Ansible and the required dependencies : 

Update your system's package manager : `sudo apt update`

Install Python and pip (if not already installed): `sudo apt install -y python3 python3-pip`

Install Ansible and other dependencies using pip: `sudo pip3 install ansible`

Install additional dependencies for Ansible modules that you may require (e.g., SSH connection support, VMware modules, etc.): `sudo pip3 install paramiko pyvmomi`

Verify the installation by checking the Ansible version: `ansible --version`

### Ansible configuration

Create and save the playbook `mellanox_switch.yml`

### Replace these placeholders with the appropriate values on mellanox_switch.yml :

`<vlan_id>`

`<vlan_name>`

`<switch_ip>`

`<switch_username>`

`<switch_password>`

`<switch_port1>`

`<switch_port2>`

### Create a shell script to execute both Terraform and Ansible :

`deploy.sh`

Replace `switch1_ip_address` and `switch2_ip_address` with the IP addresses of your Mellanox switches.

You can list multiple IP addresses separated by commas.

### Make the shell script executable: 

`chmod +x deploy.sh`

### Run the shell script to execute the deployment: 

`./deploy.sh`
