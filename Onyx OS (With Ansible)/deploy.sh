#!/bin/bash

# Deploy VLAN on vCenter using Terraform
terraform init
terraform apply -auto-approve

# Configure Mellanox switches using Ansible
ansible-playbook -i "switch1_ip_address, switch2_ip_address" mellanox_switch.yml
