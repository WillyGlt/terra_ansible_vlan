# Deploy Vlans on Vcenter and Mellanox switch (Cumulus OS) with Terraform

### Replace the placeholders with the appropriate values on vcenter_vlan.tf :

`<vcenter_username>`

`<vcenter_password>`

`<vcenter_server>`

`<vcenter_dvswitch_uuid>`

`<vcenter_dvswitch_name>`

### Install the required Terraform provider for Cumulus Networks. You can use the Cumulus Networks Terraform provider available on the official Terraform Registry. Run the following command to initialize the provider : 

`terraform init`


### Mellanox configuration

Create and save the playbook Create a new Terraform script to deploy the VLAN on the Mellanox switch with Cumulus OS. 

Save it as `mellanox_switch.tf`

### Replace these placeholders with the appropriate values on mellanox_switch.tf :

`<switch_username>`

`<switch_password>`

`<path_to_ssh_key>`

`<switch1_ip>`

`<switch2_ip>`

`<switch3_ip>`

`<switch4_ip>`

### Specify the values for the variables used in the script:

`variables.tfvars`

### Initialize the Cumulus Networks provider :

`terraform init`

### Deploy the VLAN on both vCenter and the Mellanox switch :

`terraform apply -var-file="variables.tfvars" -auto-approve`
