vlan_id          = 100
vlan_name        = "my-vlan"
switch_username  = "your_switch_username"
switch_password  = "your_switch_password"
path_to_ssh_key  = "/path/to/ssh/key.pem"
switch_ips       = ["switch1_ip", "switch2_ip", "switch3_ip", "switch4_ip"]
