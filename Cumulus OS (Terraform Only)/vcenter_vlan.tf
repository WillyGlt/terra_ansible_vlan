# Define the provider configuration for vSphere
provider "vsphere" {
  user                 = "<vcenter_username>"
  password             = "<vcenter_password>"
  vsphere_server       = "<vcenter_server>"
  allow_unverified_ssl = true
}

# Define the VLAN configuration
variable "vlan_id" {
  description = "VLAN ID"
  default     = 100
}

variable "vlan_name" {
  description = "VLAN Name"
  default     = "my-vlan"
}

# Create the VLAN on the vCenter
resource "vsphere_distributed_virtual_switch_vlan" "vlan" {
  vlan_id            = var.vlan_id
  vlan_name          = var.vlan_name
  switch_uuid        = "<vcenter_dvswitch_uuid>"
  distributed_switch = "<vcenter_dvswitch_name>"
}

# Output the VLAN information
output "vlan_info" {
  value = {
    vlan_id   = var.vlan_id
    vlan_name = var.vlan_name
  }
}
