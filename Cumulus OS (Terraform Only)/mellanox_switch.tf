# Define the provider configuration for Cumulus Networks
provider "cumulusnetworks" {
  username = "<switch_username>"
  password = "<switch_password>"
  ssh_key  = "<path_to_ssh_key>"
}

# Define the VLAN configuration
variable "vlan_id" {
  description = "VLAN ID"
  default     = 100
}

variable "vlan_name" {
  description = "VLAN Name"
  default     = "my-vlan"
}

# List of switch IP addresses
variable "switch_ips" {
  description = "List of Mellanox switch IP addresses"
  type        = list(string)
  default     = ["<switch1_ip>", "<switch2_ip>", "<switch3_ip>", "<switch4_ip>"]
}

# Create the VLAN on each Mellanox switch
resource "cumulusnetworks_vlan" "vlan" {
  count    = length(var.switch_ips)
  vlan_id  = var.vlan_id
  vlan_name = var.vlan_name

  connection {
    host     = element(var.switch_ips, count.index)
    username = var.switch_username
    password = var.switch_password
    ssh_key  = var.path_to_ssh_key
  }
}
